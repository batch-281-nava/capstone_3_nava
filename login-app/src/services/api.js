import axios from 'axios';

// ITO YUNG PINAKA FUNCTION SA PAG CALL NG API NI BACKEND

const API_BASE_URL = 'http://localhost:5000/api'; // Replace with your API base URL

const api = axios.create({
  baseURL: API_BASE_URL,
});

export const login = async (email, password) => {
  const response = await api.post('/user/register', { email, password });
  return response.data;
};

// Add other API functions as needed

export default api;
