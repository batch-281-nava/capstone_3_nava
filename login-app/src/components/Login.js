import React, { useState } from 'react';
// import { useHistory } from 'react-router-dom';
import { login } from '../services/api';

const Login = () => {
  const [userInfo, setUserInfo] = useState({
    email: "",
    password: "",
    confirmPassword: "",
    credentials: "",
    isSuccess: null
  })
  // const history = useHistory();

  const handleChange = (event) => {
    const { name, value } = event.target
    setUserInfo(previousValue => {
      return { ...previousValue, [name]: value }
    })
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    const { email, password, confirmPassword } = userInfo



    try {
      if(password !== confirmPassword){
        setUserInfo(previousValue => {
          return {...previousValue, password: "", confirmPassword: "", isSuccess: false}
        })
      } else {
      // ITO YUNG CALL PAPUNTANG BACKEND
      // YUNG NASA LOOB NG LOGIN WHICH IS SI EMAIL AT PASSWORD, TAWAG JAN PARAMETERS
      // const response = await login(email, password);
      // // ONCE SUCCESS YUNG CALL NATIN NG API KAY BACKEND, SI BACKEND MAGRERETURN YAN NG DATA
      // // YANG "user" RETURN NI REGISTER API YAN NA GALING BACKEND
      // const { user } = response
      // // SET EMAIL AND PASSWORD INPUT TO EMPTY
      // setUserInfo({
      //   email: "",
      //   password: "",
      //   confirmPassword: "",
      //   credentials: user,
      //   isSuccess: null
      // })

      // Assuming the login response includes a token
      // const token = response.data.token;

      // Store the token in local storage or a cookie for future requests
      // localStorage.setItem('token', token);

      // Redirect to the desired page
      // history.push('/dashboard');
      }
    } catch (error) {
      console.log('Login failed:', error);
    }
  };

  return (
    <div>
      <h2>REGISTER</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label>Email:</label>
          <input 
            type="email"
            name="email"
            value={userInfo.email} 
            onChange={handleChange} 
          />
        </div>
        <div>
          <label>Password:</label>
          <input 
            type="password"
            name="password"
            value={userInfo.password} 
            onChange={handleChange} />
        </div>
        <div>
          <label>Confirm Password:</label>
          <input 
            type="password"
            name="confirmPassword"
            value={userInfo.confirmPassword} 
            onChange={handleChange} />
        </div>
        <button>Login</button>
      </form>

      {userInfo.credentials && <h1>Hi {userInfo.credentials.email}</h1>}
      {userInfo.isSuccess === false && <h1>Your password does not match</h1>}
    </div>
  );
};

export default Login;
